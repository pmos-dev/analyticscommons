//declare module 'memcached-promisify' {
//}

//declare class Cache {
//	constructor(config: {}, options?: {});
//	
//	get(index: string): any;
//	set(index: string, data: any, time: number): any;
//}
//
//export = Cache;


declare module 'memcached-promisify' {
	namespace Cache {}
	
	class Cache {
		constructor(config: {}, options?: {});
		
		get(index: string): any;
		set(index: string, data: any, time: number): any;
	}
	
	export = Cache;
}
