function csoDigitToBase62(digit: number): string {
	if (digit < 10) return digit.toString();

	digit -= 10;
	if (digit < 26) return String.fromCharCode(65 + digit);
	
	digit -= 26;
	return String.fromCharCode(97 + digit);
}

function csoGenerateRandomBase62Id(): string {
	let sb: string = '';
	for (let i: number = 8; i-- > 0;) {
		const digit: number = Math.floor(Math.random() * 62);
		sb = `${sb}${csoDigitToBase62(digit)}`;
	}

	return sb;
}

function csoAddTrackingCode(trackingServer: string): string {
	const uid: string = csoGenerateRandomBase62Id();

	// add image
	const image: any = document.createElement('img');
	image.setAttribute('src', `${trackingServer}capture/${uid}.png`);
	image.setAttribute('alt', '');
	image.setAttribute('style', 'position:fixed;top:0;left:0;width:1px;height:1px;');
	document.body.appendChild(image);

	// add script
	const script: any = document.createElement('script');
	script.setAttribute('type', 'text/javascript');
	script.setAttribute('src', `${trackingServer}js/capture.js`);
	document.body.appendChild(script);
	
	return uid;
}

// the ANALYTICS_URL should be set prior to loading this script
// @ts-ignore
var ANALYTICS_URL: string = ANALYTICS_URL || '';
var ANALYTICS_UID: string = csoAddTrackingCode(ANALYTICS_URL);
