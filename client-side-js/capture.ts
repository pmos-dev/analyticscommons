function digitToBase62(digit: number): string {
	if (digit < 10) return digit.toString();

	digit -= 10;
	if (digit < 26) return String.fromCharCode(65 + digit);
	
	digit -= 26;
	return String.fromCharCode(97 + digit);
}

function generateRandomBase62Id(): string {
	let sb: string = '';
	for (let i: number = 8; i-- > 0;) {
		const digit: number = Math.floor(Math.random() * 62);
		sb = `${sb}${digitToBase62(digit)}`;
	}

	return sb;
}

function analyticsGetY(): number {
	if (typeof window.scrollY !== 'undefined') return window.scrollY;
	else if (typeof document.body !== 'undefined' && typeof document.body.scrollTop !== 'undefined') return document.body.scrollTop;
	else if (typeof document.documentElement !== 'undefined' && typeof document.documentElement.scrollTop !== 'undefined') return document.documentElement.scrollTop;
	else if (typeof window.pageYOffset !== 'undefined') return window.pageYOffset;

	return -1;
}

function analyticsGo(uid: string, url: string): void {
	const HAS_BEACON_SUPPORT: boolean = navigator !== undefined
			&& 'object' === typeof navigator
			&& navigator.sendBeacon !== undefined
			&& 'function' === typeof navigator.sendBeacon;
	
	const HAS_LOCAL_STORAGE_SUPPORT: boolean = window !== undefined
			&& 'object' === typeof window
			&& window.localStorage !== undefined;
	
	let sessionId: string|undefined;

	if (HAS_LOCAL_STORAGE_SUPPORT) {
		const existing: unknown = window.localStorage.getItem('ANALYTICS_SESSION_ID');
		
		if (existing !== undefined && 'string' === typeof existing) {
			sessionId = existing;
		} else {
			sessionId = generateRandomBase62Id();
			window.localStorage.setItem('ANALYTICS_SESSION_ID', sessionId);
		}
	}
	
	if (sessionId !== undefined) {
		const associateUrl: string = `${url}associate/${uid}/${sessionId}`;
	
		if (HAS_BEACON_SUPPORT) {
			// use Beacon API for analytics
			navigator.sendBeacon(associateUrl, '');
		} else {
			// use async AJAX requests for analytics
			const request: XMLHttpRequest = new XMLHttpRequest();
			request.open('GET', associateUrl, true);
			request.send('');
		}
	}

	{	// scope
		const deviceWidth: number = window.screen.width || 0;
		const deviceHeight: number = window.screen.height || 0;
		const viewportWidth: number = document.documentElement.clientWidth || 0;
		const viewportHeight: number = document.documentElement.clientHeight || 0;
		const sizesUrl: string = `${url}sizes/${uid}/${deviceWidth}/${deviceHeight}/${viewportWidth}/${viewportHeight}`;
	
		if (HAS_BEACON_SUPPORT) {
			// use Beacon API for analytics
			navigator.sendBeacon(sizesUrl, '');
		} else {
			// use async AJAX requests for analytics
			const request: XMLHttpRequest = new XMLHttpRequest();
			request.open('GET', sizesUrl, true);
			request.send('');
		}
	}
	
	const unloadUrl: string = `${url}unload/${uid}/`;

	if (HAS_BEACON_SUPPORT) {
		// use Beacon API for analytics

		window.addEventListener(
				'unload', (): void => {
					const y: number = analyticsGetY();
					navigator.sendBeacon(`${unloadUrl}${y === -1 ? 'na' : y.toString()}`, '');
				},
				false
		);
	} else {
		// use async AJAX requests for analytics

		window.onbeforeunload = (): void => {
			const y: number = analyticsGetY();
			const request: XMLHttpRequest = new XMLHttpRequest();
			request.open('GET', `${unloadUrl}${y === -1 ? 'na' : y.toString()}`, true);
			request.send('');
		};
	}
}

function analyticsXffPatch(uid: string, xffPatchUrl: string): void {
	const HAS_BEACON_SUPPORT: boolean = navigator !== undefined
			&& 'object' === typeof navigator
			&& navigator.sendBeacon !== undefined
			&& 'function' === typeof navigator.sendBeacon;

	// IP is worked out server side
	const patchUrl: string = `${xffPatchUrl}xff-patch/${uid}`;

	if (HAS_BEACON_SUPPORT) {
		// use Beacon API for analytics
		navigator.sendBeacon(patchUrl, '');
	} else {
		// use async AJAX requests for analytics
		const request: XMLHttpRequest = new XMLHttpRequest();
		request.open('POST', patchUrl, true);
		request.send('');
	}
}

// use a 1 second delay to allow the image request to be sent first
setTimeout(
		(): void => {
			// @ts-ignore
			const analyticsUid: string = ANALYTICS_UID || undefined;
			
			// @ts-ignore
			const analyticsUrl: string = ANALYTICS_URL || undefined;
			
			// @ts-ignore
			const analyticsXffPatchUrl: string = ANALYTICS_XFF_PATCH_URL || undefined;
			
			if (analyticsUid !== undefined && analyticsUrl !== undefined) {
				analyticsGo(analyticsUid, analyticsUrl);
			}
			if (analyticsUid !== undefined && analyticsXffPatchUrl !== undefined) {
				analyticsXffPatch(analyticsUid, analyticsXffPatchUrl);
			}
		},
		1000
);
