import { CommonsType } from 'tscommons-core';
import { IAnalyticsHit } from 'tscommons-analytics';
import { IAnalyticsLiveHit } from 'tscommons-analytics';
import { IAnalyticsLiveAssociate } from 'tscommons-analytics';

import { CommonsExpressServer } from 'nodecommons-express';
import { ICommonsExpressConfig } from 'nodecommons-express';
import { CommonsSocketIoServer } from 'nodecommons-socket-io';

export abstract class AnalyticsLiveServer<H extends IAnalyticsHit, L extends IAnalyticsLiveHit> extends CommonsSocketIoServer {
	constructor(
			expressServer: CommonsExpressServer,
			expressConfig: ICommonsExpressConfig
	) {
		super(
				expressServer,
				true,
				CommonsSocketIoServer.buildDefaultOptions(expressConfig)
		);
	}
	
	protected abstract extendLiveHit(base: IAnalyticsLiveHit, hit: H): L;
	
	public hit(hit: H, command = 'hit'): void {
		const base: IAnalyticsLiveHit = {
			timestamp: hit.timestamp
		};
		
		if (hit.session) base.session = hit.session;
		if (hit.geo) base.geo = hit.geo;
		
		const extended: L = this.extendLiveHit(base, hit);
		super.broadcast(command, CommonsType.encodePropertyObject(extended));
	}
	
	public associate(uid: string, session: string): void {
		const liveAssociate: IAnalyticsLiveAssociate = {
				uid: uid,
				session: session
		};

		super.broadcast('associate', CommonsType.encodePropertyObject(liveAssociate));
	}
	
	public chain(hit: H): void {
		this.hit(hit, 'chain');
	}
}
