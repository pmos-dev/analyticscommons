export enum EAnalyticsGeoLogicDirection {
		INCLUDE = 'include',
		EXCLUDE = 'exclude'
}
