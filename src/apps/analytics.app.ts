import { TPropertyObject } from 'tscommons-core';
import { IAnalyticsHit } from 'tscommons-analytics';
import { IAnalyticsLiveHit } from 'tscommons-analytics';
import { IAnalyticsGeo } from 'tscommons-analytics';

import { CommonsOutput } from 'nodecommons-cli';
import { CommonsFile } from 'nodecommons-file';
import { CommonsExpressServer } from 'nodecommons-express';
import { ICommonsExpressConfig, isICommonsExpressConfig } from 'nodecommons-express';
import { ICommonsRequestWithStrictParams } from 'nodecommons-express';
import { CommonsRestServer } from 'nodecommons-rest';
import { CommonsRestApp } from 'nodecommons-app-rest';

import { AnalyticsCaptureApi } from '../apis/analytics-capture.api';
import { AnalyticsExtendedApi } from '../apis/analytics-extended.api';
import { AnalyticsStatisticsApi } from '../apis/analytics-statistics.api';
import { AnalyticsXffPatchApi } from '../apis/analytics-xff-patch.api';

import { AnalyticsGeoIp } from '../classes/analytics-geo-ip';
import { AnalyticsSessionHitChain } from '../classes/analytics-session-hit-chain';

import { AnalyticsLiveServer } from '../servers/analytics-live.server';

import { AnalyticsDatabaseService } from '../services/analytics-database.service';

import { IAnalyticsCaptureConfig, isIAnalyticsCaptureConfig } from '../interfaces/ianalytics-capture-config';
import { IAnalyticsXffPatchConfig, isIAnalyticsXffPatchConfig } from '../interfaces/ianalytics-xff-patch-config';

import { TAnalyticsExtendedSizes } from '../types/tanalytics-extended-sizes';

export abstract class AnalyticsApp<H extends IAnalyticsHit, L extends IAnalyticsLiveHit, D extends AnalyticsDatabaseService<H>, LS extends AnalyticsLiveServer<H, L>> extends CommonsRestApp {
	public static getJsSourcePaths(
			name: string,
			captureConfig: IAnalyticsCaptureConfig
	): string[] {
		const attempts: string[] = [];

		if (captureConfig.captureJsPath) {
			CommonsOutput.debug(`Calculated js source from config is ${captureConfig.captureJsPath}`);
			if (CommonsFile.isDirectory(captureConfig.captureJsPath)) attempts.push(captureConfig.captureJsPath);
		}

		const cwdRelative: string = CommonsFile.cwdRelativeOrAbsolutePath('js');
		CommonsOutput.debug(`Calculated cwd node_modules path for js is ${cwdRelative}`);
		if (CommonsFile.isDirectory(cwdRelative)) attempts.push(cwdRelative);

		const localNodeModulesRelative: string = CommonsFile.rootRelativePath(`node_modules/${name}/js`);
		CommonsOutput.debug(`Calculated local node_modules path for js is ${localNodeModulesRelative}`);
		if (CommonsFile.isDirectory(localNodeModulesRelative)) attempts.push(localNodeModulesRelative);

		const globalNodeModulesRelative: string = CommonsFile.rootRelativePath('js');
		CommonsOutput.debug(`Calculated global node_modules path for js is ${globalNodeModulesRelative}`);
		if (CommonsFile.isDirectory(globalNodeModulesRelative)) attempts.push(globalNodeModulesRelative);

		CommonsOutput.debug(`Searching for js in paths: ${attempts.join('; ')}`);
		
		return attempts;
	}

	private geoIp: AnalyticsGeoIp;
	private databaseService: D|undefined;
	private liveServer: LS|undefined;
	
	private _expressConfig: ICommonsExpressConfig;
	private captureJsCode: string;
	
	protected captureConfig: IAnalyticsCaptureConfig;
	private xffPatchConfig: IAnalyticsXffPatchConfig|undefined;
	
	constructor(
			name: string,
			private useChain: boolean = false,
			private debug: boolean = false
	) {
		super(
				name,
				true
		);

		const captureConfig: TPropertyObject = this.getConfigArea('capture');
		if (!isIAnalyticsCaptureConfig(captureConfig)) throw new Error('Invalid capture config');
		
		const captureJsCodePaths: string[] = AnalyticsApp.getJsSourcePaths(name, captureConfig);
		if (captureJsCodePaths.length === 0) throw new Error('Unable to locate js capture source');

		captureConfig.captureJsPath = captureJsCodePaths[0];
		CommonsOutput.debug(`Found js capture path in: ${captureConfig.captureJsPath}`);
		
		this.captureConfig = captureConfig;
		
		const captureJsCode: string|undefined = CommonsFile.readTextFile(`${captureConfig.captureJsPath}/capture.js`);
		if (captureJsCode === undefined) throw new Error('Unable to read capture code file');
		
		this.captureJsCode = captureJsCode;

		this.geoIp = new AnalyticsGeoIp();

		const expressConfig: TPropertyObject = this.getConfigArea('express');
		if (!isICommonsExpressConfig(expressConfig)) throw new Error('Express config is not valid');
		
		this._expressConfig = expressConfig;
		
		const xffPatchConfig: unknown = this.getConfigArea('xffPatch');
		if (isIAnalyticsXffPatchConfig(xffPatchConfig)) {
			this.xffPatchConfig = xffPatchConfig;
		}
	}
	
	protected abstract buildLiveServer(
			expressServer: CommonsExpressServer,
			expressConfig: ICommonsExpressConfig
	): LS;
	
	protected getLiveServer(): LS {
		if (!this.liveServer) throw new Error('The LiveServer has not been instantiated yet');
		return this.liveServer;
	}
	
	public setDatabaseService(
			databaseService: D
	): void {
		this.databaseService = databaseService;
	}
	
	protected getDatabaseService(): D {
		if (!this.databaseService) throw new Error('The DatabaseService has not been instantiated yet');
		return this.databaseService;
	}
	
	protected abstract isAccepted(base: IAnalyticsHit, request: ICommonsRequestWithStrictParams): boolean;
	protected abstract extendHit(base: IAnalyticsHit, request: ICommonsRequestWithStrictParams): H;

	public async init(): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set yet');
		
		CommonsOutput.doing('Connecting to database');
		await this.databaseService.init();
		CommonsOutput.success();
		
		CommonsOutput.doing('Initialising GeoIP');
		await this.geoIp.load();
		CommonsOutput.success();
		
		CommonsOutput.doing('Building live server');
		this.liveServer = this.buildLiveServer(
				this.getExpressServer(),
				this._expressConfig
		);
		CommonsOutput.success();
		
		this.install(
				(restServer: CommonsRestServer, path: string): void => {
					CommonsOutput.doing('Installing capture API');
					const apiCapture: AnalyticsCaptureApi<H> = new AnalyticsCaptureApi<H>(
							restServer,
							this.geoIp,
							this.captureJsCode,
							path,
							(base: IAnalyticsHit, request: ICommonsRequestWithStrictParams): boolean => {
								return this.isAccepted(base, request);
							},
							(base: IAnalyticsHit, request: ICommonsRequestWithStrictParams): H => {
								return this.extendHit(base, request);
							},
							(hit: H): Promise<boolean> => {	// log hit
								return this.logHit(hit);
							},
							(	// associateHitWithSession
									uid: string,
									session: string
							): Promise<boolean> => {
								return this.associateHitWithSession(uid, session);
							},
							(	// logUnload
									uid: string,
									y: number|undefined
							): Promise<boolean> => {
								return this.logUnload(uid, y);
							}
					);
					if (this.debug) apiCapture.setDebug(true);
					
					// have to do this directly with CommonsExpressServer, as the REST methods only work with JSON
					apiCapture.installRequestHandlers(this.getExpressServer());

					apiCapture.addAcceptedHitCallback(
							(hit: H): void => {
								this.getLiveServer().hit(hit);
							}
					);

					apiCapture.addAssociateCallback(
							(uid: string, session: string): void => {
								this.getLiveServer().associate(uid, session);
							}
					);
					CommonsOutput.success();
					
					if (this.useChain) {
						CommonsOutput.doing('Installing association chain support');
						const chain: AnalyticsSessionHitChain<H> = new AnalyticsSessionHitChain<H>(
								(hit: H): void => {
									this.getLiveServer().chain(hit);
								}
						);
						apiCapture.addAcceptedHitCallback(
								(hit: H): void => {
									chain.queue(hit);
								}
						);
	
						apiCapture.addAssociateCallback(
								(uid: string, session: string): void => {
									chain.associate(uid, session);
								}
						);
						CommonsOutput.success();
					}
					
					CommonsOutput.doing('Installing extended API');
					const apiExtended: AnalyticsExtendedApi = new AnalyticsExtendedApi(
							restServer,
							path,
							(	// setSizes
									uid: string,
									sizes: TAnalyticsExtendedSizes
							): Promise<boolean> => {
								return this.setSizes(uid, sizes);
							}
					);
					if (this.debug) apiExtended.setDebug(true);
					CommonsOutput.success();
					
					CommonsOutput.doing('Installing statistics API');
					// tslint:disable-next-line:no-unused-expression
					new AnalyticsStatisticsApi<H>(
							restServer,
							this.getDatabaseService(),
							path
					);
					CommonsOutput.success();

					if (this.xffPatchConfig) {
						if (this.xffPatchConfig.ignoreIps && this.xffPatchConfig.ignoreIps.length > 0) {
							apiCapture.setIgnoreIps(this.xffPatchConfig.ignoreIps);
						}
						
						CommonsOutput.doing('Installing XFF patch API');
						const apiXffPatch: AnalyticsXffPatchApi = new AnalyticsXffPatchApi(
								restServer,
								path,
								undefined,
								async (uid: string, ip: string): Promise<boolean> => {
									await this.xffPatchReplaceIp(uid, ip);
									return true;
								}
						);
						if (this.debug) apiXffPatch.setDebug(true);
						CommonsOutput.success();
					}
				}
		);
		
		await super.init();
	}
	
	private logHit(hit: H): Promise<boolean> {
		return this.getDatabaseService().log(hit);
	}
	
	private associateHitWithSession(uid: string, session: string): Promise<boolean> {
		return this.getDatabaseService().associate(uid, session);
	}
	
	private logUnload(uid: string, y: number|undefined): Promise<boolean> {
		return this.getDatabaseService().setUnload(uid, y);
	}
	
	private setSizes(uid: string, sizes: TAnalyticsExtendedSizes): Promise<boolean> {
		return this.getDatabaseService().setSizes(uid, sizes);
	}
	
	private async xffPatchReplaceIp(uid: string, ip: string): Promise<void> {
		const geo: IAnalyticsGeo|undefined = this.geoIp.getGeo(ip);
		if (geo) await this.getDatabaseService().setGeo(uid, geo);
	}
	
	public async run(): Promise<void> {
		await super.run();
	}
}
