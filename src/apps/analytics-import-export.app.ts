import { IAnalyticsHit } from 'tscommons-analytics';

import { CommonsOutput } from 'nodecommons-cli';
import { CommonsApp } from 'nodecommons-app';

import { AnalyticsDatabaseService } from '../services/analytics-database.service';

export abstract class AnalyticsImportExportApp<H extends IAnalyticsHit, D extends AnalyticsDatabaseService<H>> extends CommonsApp {
	private databaseService: D|undefined;

	constructor(
			protected filename: string,
			name: string
	) {
		super(name);
	}
	
	public setDatabaseService(
			databaseService: D
	): void {
		this.databaseService = databaseService;
	}
	
	protected getDatabaseService(): D {
		if (!this.databaseService) throw new Error('The DatabaseService has not been instantiated yet');
		return this.databaseService;
	}
	
	public async init(): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set yet');
		
		CommonsOutput.doing('Connecting to database');
		await this.databaseService.init();
		CommonsOutput.success();
		
		await super.init();
	}
}
