import { IAnalyticsHit } from 'tscommons-analytics';
import { IAnalyticsLiveHit } from 'tscommons-analytics';

import { CommonsOutput } from 'nodecommons-cli';
import { CommonsFile } from 'nodecommons-file';
import { CommonsRestServer } from 'nodecommons-rest';

import { AnalyticsClientSideOnlyCaptureApi } from '../apis/analytics-client-side-only-capture.api';

import { AnalyticsLiveServer } from '../servers/analytics-live.server';

import { AnalyticsDatabaseService } from '../services/analytics-database.service';

import { AnalyticsApp } from './analytics.app';

class ClientSideOnlyCaptureApi extends AnalyticsClientSideOnlyCaptureApi {
	constructor(
			restServer: CommonsRestServer,
			clientSideOnlyCaptureJsCode: string,
			path: string
	) {
		super(
				restServer,
				clientSideOnlyCaptureJsCode,
				path
		);
	}
}

export abstract class AnalyticsClientSideOnlyApp<H extends IAnalyticsHit, L extends IAnalyticsLiveHit, D extends AnalyticsDatabaseService<H>, LS extends AnalyticsLiveServer<H, L>> extends AnalyticsApp<H, L, D, LS> {
	constructor(
			name: string,
			useChain: boolean = false,
			debug: boolean = false
	) {
		super(
				name,
				useChain,
				debug
		);
		
		const clientSideOnlyCaptureJsCode: string|undefined = CommonsFile.readTextFile(`${this.captureConfig.captureJsPath}/client-side-only.js`);
		if (clientSideOnlyCaptureJsCode === undefined) throw new Error('Unable to read capture code file');

		this.install(
				(restServer: CommonsRestServer, path: string): void => {
					CommonsOutput.doing('Installing client-side-only capture API');
					
					// tslint:disable-next-line:no-unused-expression
					const api: ClientSideOnlyCaptureApi = new ClientSideOnlyCaptureApi(
							restServer,
							clientSideOnlyCaptureJsCode,
							path
					);
					
					// have to do this directly with CommonsExpressServer, as the REST methods only work with JSON
					api.installRequestHandlers(this.getExpressServer());
					
					CommonsOutput.success();
				}
		);
	}
}
