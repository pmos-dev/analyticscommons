import { IAnalyticsHit } from 'tscommons-analytics';

import { CommonsOutput } from 'nodecommons-cli';

import { AnalyticsExport } from '../classes/analytics-export';

import { AnalyticsDatabaseService } from '../services/analytics-database.service';

import { AnalyticsImportExportApp } from './analytics-import-export.app';

export abstract class AnalyticsImportApp<H extends IAnalyticsHit, D extends AnalyticsDatabaseService<H>> extends AnalyticsImportExportApp<H, D> {
	protected abstract isH(test: unknown): test is H;

	public async import(
			filename: string
	): Promise<void> {
		const impoter: AnalyticsExport<H> = new AnalyticsExport<H>(this.getDatabaseService());
		
		CommonsOutput.doing('Importing hits from file');
		
		const [ success, errors ] = await impoter.import(
				filename,
				this.isH,
				(s: number, e: number): void => {
					CommonsOutput.progress(`${s} succeeded, ${e} errors`);
				}
		);
		CommonsOutput.result(`${success} succeeded, ${errors} errors`);
	}
	
	public async run(): Promise<void> {
		await this.import(this.filename);
	}
}
