import { AnalyticsRestClientService } from 'tscommons-analytics';

import { CommonsOutput } from 'nodecommons-cli';
import { CommonsInternalHttpClientImplementation } from 'nodecommons-http';
import { CommonsRestServer } from 'nodecommons-rest';
import { CommonsRestApp } from 'nodecommons-app-rest';

import { AnalyticsXffPatchApi } from '../apis/analytics-xff-patch.api';

import { IAnalyticsXffPatchConfig, isIAnalyticsXffPatchConfig } from '../interfaces/ianalytics-xff-patch-config';

export class AnalyticsXffPatchApp extends CommonsRestApp {
	protected xffPatchConfig: IAnalyticsXffPatchConfig;
	
	private restClientService: AnalyticsRestClientService<any>;
	
	constructor(
			name: string,
			private debug: boolean = false
	) {
		super(
				name,
				true
		);

		const xffPatchConfig: unknown = this.getConfigArea('xffPatch');
		if (!isIAnalyticsXffPatchConfig(xffPatchConfig)) throw new Error('Invalid Xff Patch config');
		
		this.xffPatchConfig = xffPatchConfig;
		if (!this.xffPatchConfig.analyticsUrl) throw new Error('No analyticsUrl set in the xffPatch config');
		
		this.restClientService = new AnalyticsRestClientService<any>(
				new CommonsInternalHttpClientImplementation(),
				this.xffPatchConfig.analyticsUrl
		);
	}
	
	public async init(): Promise<void> {
		this.install(
				(restServer: CommonsRestServer, path: string): void => {
					CommonsOutput.doing('Installing XFF patch API');
					const xffPatchApi: AnalyticsXffPatchApi = new AnalyticsXffPatchApi(
							restServer,
							path,
							async (uid: string, ip: string): Promise<boolean> => {
								return await this.patch(uid, ip);
							},
							undefined
					);
					if (this.debug) xffPatchApi.setDebug(true);
				}
		);
		
		await super.init();
	}
	
	private async patch(uid: string, ip: string): Promise<boolean> {
		await this.restClientService.xffPatchIp(uid, ip);
		
		return true;
	}
}
