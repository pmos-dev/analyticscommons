import { IAnalyticsHit } from 'tscommons-analytics';

import { CommonsOutput } from 'nodecommons-cli';

import { AnalyticsExport } from '../classes/analytics-export';

import { AnalyticsDatabaseService } from '../services/analytics-database.service';

import { AnalyticsImportExportApp } from './analytics-import-export.app';

export class AnalyticsExportApp<H extends IAnalyticsHit, D extends AnalyticsDatabaseService<H>> extends AnalyticsImportExportApp<H, D> {
	public async export(filename: string): Promise<void> {
		const exporter: AnalyticsExport<H> = new AnalyticsExport<H>(this.getDatabaseService());
		
		CommonsOutput.doing('Exporting all hits');
		
		const tally = await exporter.export(
				filename,
				(t: number): void => {
					CommonsOutput.progress(t);
				}
		);
		CommonsOutput.result(tally);
	}
	
	public async run(): Promise<void> {
		await this.export(this.filename);
	}
}
