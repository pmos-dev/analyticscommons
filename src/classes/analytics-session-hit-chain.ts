import { IAnalyticsHit } from 'tscommons-analytics';

export class AnalyticsSessionHitChain<H extends IAnalyticsHit> {
	private hits: H[] = [];
	
	constructor(
			private callback: (hit: H) => void
	) {}
	
	public queue(hit: H): void {
		this.hits.push(hit);
		
		if (this.hits.length > 1000) this.hits.shift();
	}

	public associate(uid: string, session: string): void {
		const match: H|undefined = this.hits
				.find((h: H): boolean => h.uid === uid);
		if (!match) return;
		
		const index: number = this.hits.indexOf(match);
		this.hits.splice(index);
		
		match.session = session;
		this.callback(match);
	}
}
