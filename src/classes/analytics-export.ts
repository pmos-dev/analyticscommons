import * as fs from 'fs';
import * as readline from 'readline';

import { Cursor } from 'mongodb';

import { CommonsType } from 'tscommons-core';
import { CommonsObject } from 'tscommons-core';
import { CommonsArray } from 'tscommons-core';
import { IAnalyticsHit } from 'tscommons-analytics';

import { AnalyticsDatabaseService } from '../services/analytics-database.service';

const ISO_DATE_STRING: RegExp = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*))(?:Z|(\+|-)([\d|:]*))?$/;

export class AnalyticsExport<H extends IAnalyticsHit> {
	constructor(
			private databaseService: AnalyticsDatabaseService<H>
	) {}
	
	private async exportTo(
			writeStream: fs.WriteStream,
			progress?: (tally: number) => void
	): Promise<number> {
		const hits: Cursor<H> = this.databaseService.export();
		
		let tally: number = 0;
		
		await this.databaseService.forEachQueryResults(
				hits,
				(_t: unknown): _t is H => true,	// we presume exports are always valid
				async (hit: H): Promise<void> => {
					writeStream.write(JSON.stringify(hit));
					writeStream.write('\n');
					
					tally++;
					if ((tally % 100) === 0 && progress) progress(tally);
				}
		);
		
		await hits.close();
		
		return tally;
	}
	
	public export(
			filename: string,
			progress?: (tally: number) => void
	): Promise<number> {
		return new Promise((resolve: (tally: number) => void, reject: (reason: Error) => void): void => {
			const writeStream: fs.WriteStream = fs.createWriteStream(filename);
			
			writeStream.on(
					'error',
					(err: Error): void => {
						writeStream.close();
						reject(err);
					}
			);
			
			this.exportTo(
					writeStream,
					progress
			)
					.then((tally: number): void => {
						resolve(tally);
					})
					.finally((): void => {
						writeStream.close();
					});
		});
	}
	
	private async importFrom(
			rl: readline.Interface,
			isH: (test: unknown) => test is H,
			progress?: (
					success: number,
					errors: number
			) => void
	): Promise<[ number, number ]> {
		let success: number = 0;
		let errors: number = 0;
		
		const batch: H[] = [];
		
		for await (const line of rl) {
			try {
				const hit: unknown = JSON.parse(
						line,
						(_key: string, value: unknown): any => {
							if (value === null) return undefined;
							
							if (!CommonsType.isString(value)) return value;
							if (!ISO_DATE_STRING.test(value)) return value;
							
							return new Date(value);
						}
				);
				
				const stripped: H = CommonsObject.stripNulls(hit);
				
				if (!isH(stripped)) throw new Error('Invalid H');

				success++;
				
				batch.push(stripped);
				
				if (batch.length > 100) {
					await this.databaseService.logMany(batch);
					CommonsArray.empty(batch);
				}
			} catch (e) {
				errors++;
			}
			
			if (((success % 100) === 0) || ((errors % 100) === 0)) {
				if (progress) progress(success, errors);
			}
		}
		
		return [ success, errors ];
	}
	
	public import(
			filename: string,
			isH: (test: unknown) => test is H,
			progress?: (
					success: number,
					errors: number
			) => void
	): Promise<[ number, number ]> {
		return new Promise((resolve: (tallies: [ number, number ]) => void, _reject: (reason: Error) => void): void => {
			const readStream: fs.ReadStream = fs.createReadStream(filename);
			const rl: readline.Interface = readline.createInterface({
					input: readStream,
					crlfDelay: Infinity
			});
			
			this.importFrom(
					rl,
					isH,
					progress
			)
					.then((tallies: [ number, number ]): void => {
						resolve(tallies);
					})
					.finally((): void => {
						readStream.close();
					});
		});
	}
}
