import { UAParser } from 'ua-parser-js';

import { CommonsType } from 'tscommons-core';
import { IAnalyticsDevice } from 'tscommons-analytics';
import { parseEAnalyticsDeviceType } from 'tscommons-analytics';
import { toEAnalyticsEngine } from 'tscommons-analytics';
import { toEAnalyticsCpu } from 'tscommons-analytics';

export class AnalyticsDevice {
	
	private static decode(detection: UAParser): IAnalyticsDevice {
		const data: IAnalyticsDevice = {};
		
		const device: IUAParser.IDevice = detection.getDevice();
		if (device) {
			if (!CommonsType.isBlank(device.type)) data.type = parseEAnalyticsDeviceType(device.type);
			if (!CommonsType.isBlank(device.model)) data.model = device.model;
			if (!CommonsType.isBlank(device.vendor)) data.vendor = device.vendor;
		}
		
		const cpu: IUAParser.ICPU|undefined = detection.getCPU();
		if (cpu) {
			if (!CommonsType.isBlank(cpu.architecture)) data.cpu = toEAnalyticsCpu(cpu.architecture);
		}
		
		const os: IUAParser.IEngine|undefined = detection.getOS();
		if (os) {
			if (!CommonsType.isBlank(os.name)) data.os = os.name;
			if (!CommonsType.isBlank(os.version)) data.osVersion = parseInt(os.version, 10);
		}
		
		const engine: IUAParser.IEngine|undefined = detection.getEngine();
		if (engine) {
			if (!CommonsType.isBlank(engine.name)) data.engine = toEAnalyticsEngine(engine.name);
			if (!CommonsType.isBlank(engine.version)) data.engineVersion = parseInt(engine.version, 10);
		}
		
		const browser: IUAParser.IBrowser|undefined = detection.getBrowser();
		if (browser) {
			if (!CommonsType.isBlank(browser.name)) data.browser = browser.name;
			if (!CommonsType.isBlank(browser.version)) data.browserVersion = parseInt(browser.version, 10);
		}
		
		return data;
	}
	
	public static detect(userAgent: string|undefined): IAnalyticsDevice|undefined {
		if (!userAgent) return undefined;
		
		const detection: UAParser = new UAParser(userAgent);
		if (!detection) return undefined;
		if (Object.keys(detection).length === 0) {
			// no data, so essentiall undefined anyway
			return undefined;
		}
		
		return AnalyticsDevice.decode(detection);
	}
}
