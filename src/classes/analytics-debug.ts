import { IAnalyticsHit } from 'tscommons-analytics';

import { CommonsOutput } from 'nodecommons-cli';

import { TAnalyticsExtendedSizes } from '../types/tanalytics-extended-sizes';

export class AnalyticsDebug {
	protected enabled: boolean = false;
	
	public setEnabled(state: boolean = true): void {
		this.enabled = state;
		CommonsOutput.setDebugging(state);
	}

	public log(hit: IAnalyticsHit): void {
		if (!this.enabled) return;

		CommonsOutput.info(`Logging hit ${hit.uid} via ${hit.captureMethod}`);
	}

	public reject(): void {
		if (!this.enabled) return;

		CommonsOutput.error(`Rejecting invalid hit`);
	}

	public associate(uid: string, session: string): void {
		if (!this.enabled) return;
		
		CommonsOutput.debug(`Assocating ${uid} with ${session}`);
	}

	public unload(uid: string): void {
		if (!this.enabled) return;
		
		CommonsOutput.debug(`Storing unload details for ${uid}`);
	}

	public sizes(uid: string, sizes: TAnalyticsExtendedSizes): void {
		if (!this.enabled) return;
		
		CommonsOutput.debug(`Storing sizes details for ${uid}: ${JSON.stringify(sizes)}`);
	}
	
}
