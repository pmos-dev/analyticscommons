import maxmind, { Reader, CityResponse, CountryResponse } from 'maxmind';
import * as geolite2 from 'geolite2-redist';

import { IAnalyticsGeo, IAnalyticsCountry, IAnalyticsCity } from 'tscommons-analytics';

export class AnalyticsGeoIp {
	private lookupCity: Reader<CityResponse>|undefined;
	private lookupCountry: Reader<CountryResponse>|undefined;

	public async load(): Promise<void> {
		this.lookupCity = await maxmind.open<CityResponse>(geolite2.paths['GeoLite2-City']);
		this.lookupCountry = await maxmind.open<CountryResponse>(geolite2.paths['GeoLite2-Country']);
	}
	
	public getCity(ip: string): IAnalyticsCity|undefined {
		if (this.lookupCity === undefined) return undefined;
		
		const match: CityResponse|null = this.lookupCity.get(ip);
		if (match === null) return undefined;
		
		const data: IAnalyticsCity = {};
		
		if (
				match.city && match.city.names
				&& match.city.names.en
				&& 'string' === typeof match.city.names.en
		) data.city = match.city.names.en;

		if (
				match.country && match.country.names
				&& match.country.names.en
				&& 'string' === typeof match.country.names.en
		) data.country = match.country.names.en;
		
		if (
				match.continent && match.continent.names
				&& match.continent.names.en
				&& 'string' === typeof match.continent.names.en
		) data.continent = match.continent.names.en;

		if (
				match['postal'] && match['postal']['code']
				&& 'string' === typeof match['postal']['code']
		) data.postCode = match['postal']['code'];
		
		if (
				match['location']
				&& match['location']['latitude']
				&& 'number' === typeof match['location']['latitude']
				&& match['location']['longitude']
				&& 'number' === typeof match['location']['longitude']
				&& match['location']['accuracy_radius']
				&& 'number' === typeof match['location']['accuracy_radius']
		) {
			data.location = {
					latitude: match['location']['latitude'],
					longitude: match['location']['longitude'],
					radius: match['location']['accuracy_radius']
			};
		}

		return data;
	}
	
	public getCountry(ip: string): IAnalyticsCountry|undefined {
		if (this.lookupCountry === undefined) return undefined;
		
		const match: CountryResponse|null = this.lookupCountry.get(ip);
		if (match === null) return undefined;
		
		const data: IAnalyticsCountry = {};
		
		if (
				match.country && match.country.names
				&& match.country.names.en
				&& 'string' === typeof match.country.names.en
		) data.country = match.country.names.en;
		
		if (
				match.continent && match.continent.names
				&& match.continent.names.en
				&& 'string' === typeof match.continent.names.en
		) data.continent = match.continent.names.en;

		return data;
	}
	
	public getGeo(ip: string): IAnalyticsGeo|undefined {
		return this.getCity(ip) || this.getCountry(ip);
	}
}
