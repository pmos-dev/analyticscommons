import { CommonsType } from 'tscommons-core';

export interface IAnalyticsXffPatchConfig {
		analyticsUrl?: string;
		ignoreIps?: string[];
}

export function isIAnalyticsXffPatchConfig(test: any): test is IAnalyticsXffPatchConfig {
	if (!CommonsType.hasPropertyStringOrUndefined(test, 'analyticsUrl')) return false;
	if (!CommonsType.hasPropertyStringArrayOrUndefined(test, 'ignoreIps')) return false;

	return true;
}
