import { CommonsType } from 'tscommons-core';

export interface IAnalyticsCaptureConfig {
		captureJsPath?: string;
}

export function isIAnalyticsCaptureConfig(test: any): test is IAnalyticsCaptureConfig {
	if (!CommonsType.hasPropertyStringOrUndefined(test, 'captureJsPath')) return false;

	return true;
}
