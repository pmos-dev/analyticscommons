import * as express from 'express';

import { CommonsType } from 'tscommons-core';
import { TEncodedArray } from 'tscommons-core';
import { TDateRange } from 'tscommons-core';
import { IAnalyticsHit } from 'tscommons-analytics';
import { TAnalyticsTimestampTally } from 'tscommons-analytics';
import { TAnalyticsGeoAggregate } from 'tscommons-analytics';
import { TAnalyticsGeoHitCity } from 'tscommons-analytics';
import { TAnalyticsGeoHitCountry } from 'tscommons-analytics';
import { TAnalyticsDeviceTypeHits } from 'tscommons-analytics';
import { TAnalyticsDeviceCpuHits } from 'tscommons-analytics';
import { EAnalyticsGeoRegion, fromEAnalyticsGeoRegion, toEAnalyticsGeoRegion, EANALYTICS_GEO_REGIONS } from 'tscommons-analytics';

import { CommonsExpress } from 'nodecommons-express';
import { ICommonsRequestWithStrictParams } from 'nodecommons-express';
import { CommonsRestServer } from 'nodecommons-rest';
import { CommonsRestApi } from 'nodecommons-rest';

import { AnalyticsDatabaseService } from '../services/analytics-database.service';

import { EAnalyticsGeoLogicDirection } from '../enums/eanalytics-geo-logic-direction';

import { AnalyticsCachingApi } from './analytics-caching.api';

// ideally we should not pass in the databaseService directly, and use callbacks
// but there are too many to do at this time. maybe something for the future as an improvement

export class AnalyticsStatisticsApi<H extends IAnalyticsHit> extends AnalyticsCachingApi {
	constructor(
			restServer: CommonsRestServer,
			protected databaseService: AnalyticsDatabaseService<H>,
			path: string
	) {
		super(restServer, 'analytics-commons');

		super.getHandler(
				`${path}statistics/hits/total`,
				async (req: express.Request, _res: express.Response): Promise<number> => {
					return this.getCacheOrRenewDateRange<number>(
							'statistics/hits/total',
							10,
							async (range?: TDateRange): Promise<number> => {
								if (range) return await this.databaseService.getTotalHitsRange(range);

								return await this.databaseService.getTotalHits();
							},
							CommonsExpress.getQueryDateRange(req)
					);
				}
		);

		super.getHandler(
				`${path}statistics/hits/unique`,
				async (req: express.Request, _res: express.Response): Promise<number> => {
					return this.getCacheOrRenewDateRange<number>(
							'statistics/hits/unique',
							10,
							async (range?: TDateRange): Promise<number> => {
								if (range) return await this.databaseService.getUniqueHitsRange(range);

								return await this.databaseService.getUniqueHits();
							},
							CommonsExpress.getQueryDateRange(req)
					);
				}
		);

		super.getHandler(
				`${path}statistics/hits/unknown`,
				async (req: express.Request, _res: express.Response): Promise<number> => {
					return this.getCacheOrRenewDateRange<number>(
							'statistics/hits/unknown',
							10,
							async (range?: TDateRange): Promise<number> => {
								if (range) return await this.databaseService.getUnknownHitsRange(range);

								return await this.databaseService.getUnknownHits();
							},
							CommonsExpress.getQueryDateRange(req)
					);
				}
		);

		super.getHandler(
				`${path}statistics/hits/hourly/total`,
				async (req: express.Request, _res: express.Response): Promise<TEncodedArray> => {
					return this.getCacheOrRenewDateRange<TEncodedArray>(
							'statistics/hits/hourly/total',
							15,
							async (range?: TDateRange): Promise<TEncodedArray> => {
								if (range) return CommonsType.encode(await this.databaseService.listHourlyTotalHitsRange(range)) as TEncodedArray;

								return CommonsType.encode(await this.databaseService.listHourlyTotalHits()) as TEncodedArray;
							},
							CommonsExpress.getQueryDateRange(req)
					);
				}
		);

		super.getHandler(
				`${path}statistics/hits/daily/total`,
				async (req: express.Request, _res: express.Response): Promise<TEncodedArray> => {
					return this.getCacheOrRenewDateRange<TEncodedArray>(
							'statistics/hits/daily/total',
							15,
							async (range?: TDateRange): Promise<TEncodedArray> => {
								if (range) return CommonsType.encode(await this.databaseService.listDailyTotalHitsRange(range)) as TEncodedArray;

								return CommonsType.encode(await this.databaseService.listDailyTotalHits()) as TEncodedArray;
							},
							CommonsExpress.getQueryDateRange(req)
					);
				}
		);
		
		super.getHandler(
				`${path}statistics/hits/hourly/unknown`,
				async (req: express.Request, _res: express.Response): Promise<TEncodedArray> => {
					return this.getCacheOrRenewDateRange<TEncodedArray>(
							'statistics/hits/hourly/unknown',
							15,
							async (range?: TDateRange): Promise<TEncodedArray> => {
								if (range) return CommonsType.encode(await this.databaseService.listHourlyUnknownHitsRange(range)) as TEncodedArray;

								return CommonsType.encode(await this.databaseService.listHourlyUnknownHits()) as TEncodedArray;
							},
							CommonsExpress.getQueryDateRange(req)
					);
				}
		);

		super.getHandler(
				`${path}statistics/hits/daily/unknown`,
				async (req: express.Request, _res: express.Response): Promise<TEncodedArray> => {
					return this.getCacheOrRenewDateRange<TEncodedArray>(
							'statistics/hits/daily/unknown',
							15,
							async (range?: TDateRange): Promise<TEncodedArray> => {
								if (range) return CommonsType.encode(await this.databaseService.listDailyUnknownHitsRange(range)) as TEncodedArray;

								return CommonsType.encode(await this.databaseService.listDailyUnknownHits()) as TEncodedArray;
							},
							CommonsExpress.getQueryDateRange(req)
					);
				}
		);

		super.getHandler(
				`${path}statistics/hits/hourly/unique`,
				async (req: express.Request, _res: express.Response): Promise<TEncodedArray> => {
					return this.getCacheOrRenewDateRange<TEncodedArray>(
							'statistics/hits/hourly/unique',
							15,
							async (range?: TDateRange): Promise<TEncodedArray> => {
								// There is no easy way to calculate unique hits hourly, as the requirement to group by hour vs distinct session id creates a contradictory situation.
								// Instead, perform a regular total lookup, and then cycle through each hour returned to do a range session distinct check.
								// It's slow and inefficient, but it's the only easy way to do it.
								// The cache is useful to help with this.

								const base: TAnalyticsTimestampTally[] = [];
								
								if (range) {
									base.push(...await this.databaseService.listHourlyTotalHitsRange(range));
								} else {
									base.push(...await this.databaseService.listHourlyTotalHits());
								}
			
								const interval: number = 60 * 60 * 1000;	// 1 hour
			
								return CommonsType.encode(await this.rebuildUniqueTally(base, interval)) as TEncodedArray;
							},
							CommonsExpress.getQueryDateRange(req)
					);
				}
		);

		super.getHandler(
				`${path}statistics/hits/daily/unique`,
				async (req: express.Request, _res: express.Response): Promise<TEncodedArray> => {
					return this.getCacheOrRenewDateRange<TEncodedArray>(
							'statistics/hits/daily/unique',
							15,
							async (range?: TDateRange): Promise<TEncodedArray> => {
								// There is no easy way to calculate unique hits daily, as the requirement to group by day vs distinct session id creates a contradictory situation.
								// Instead, perform a regular total lookup, and then cycle through each day returned to do a range session distinct check.
								// It's slow and inefficient, but it's the only easy way to do it.
								// The cache is useful to help with this.

								const base: TAnalyticsTimestampTally[] = [];
								
								if (range) {
									base.push(...await this.databaseService.listDailyTotalHitsRange(range));
								} else {
									base.push(...await this.databaseService.listDailyTotalHits());
								}
			
								const interval: number = 24 * 60 * 60 * 1000;	// 24 hours
			
								return CommonsType.encode(await this.rebuildUniqueTally(base, interval)) as TEncodedArray;
							},
							CommonsExpress.getQueryDateRange(req)
					);
				}
		);

		const regions: string[] = EANALYTICS_GEO_REGIONS
				.map((region: EAnalyticsGeoRegion): string => fromEAnalyticsGeoRegion(region));
				
		super.getHandler(
				`${path}statistics/geo/map/:which(${regions.join('|')})/:granularity[int]`,
				async (req: ICommonsRequestWithStrictParams, _res: express.Response): Promise<TAnalyticsGeoAggregate[]> => {
					const granularity: number = req.strictParams.granularity as number;
					if (granularity <= 0) return [];
					
					const which: string|undefined = toEAnalyticsGeoRegion(req.strictParams.which as string);
					if (!which) return CommonsRestApi.badRequest('Invalid which. This should not be possible');

					return this.getCacheOrRenewDateRange<TAnalyticsGeoAggregate[]>(
							`statistics/geo/map/${which}/${granularity.toString(10)}`,
							60,
							async (range?: TDateRange): Promise<TAnalyticsGeoAggregate[]> => {
								const conditions: any = [];
								switch (which) {
									case EAnalyticsGeoRegion.UK:
										conditions.push(
												{ direction: EAnalyticsGeoLogicDirection.INCLUDE, field: 'geo.country', value: 'United Kingdom' }
										);
										break;
									case EAnalyticsGeoRegion.WORLD:
										conditions.push(
												{ direction: EAnalyticsGeoLogicDirection.EXCLUDE, field: 'geo.country', value: 'United Kingdom' }
										);
										break;
								}
								
								return await this.databaseService.listGeoAggregate(
										granularity,
										conditions,
										range
								);
							},
							CommonsExpress.getQueryDateRange(req)
					);
				}
		);

		super.getHandler(
				`${path}statistics/geo/hits/city/:which(${regions.join('|')})`,
				async (req: ICommonsRequestWithStrictParams, _res: express.Response): Promise<TAnalyticsGeoHitCity[]> => {
					const limit: number = CommonsType.hasPropertyNumber(req.query, 'limit') ? parseInt(req.query.limit as string, 10) : 100;
					
					const which: string|undefined = toEAnalyticsGeoRegion(req.strictParams.which as string);
					if (!which) return CommonsRestApi.badRequest('Invalid which. This should not be possible');
					
					return this.getCacheOrRenewDateRange<TAnalyticsGeoHitCity[]>(
							`statistics/geo/hits/city/${which}/${limit.toString(10)}`,
							60,
							async (range?: TDateRange): Promise<TAnalyticsGeoHitCity[]> => {
								const conditions: any = [];
								switch (which) {
									case EAnalyticsGeoRegion.UK:
										conditions.push(
												{ direction: EAnalyticsGeoLogicDirection.INCLUDE, field: 'geo.country', value: 'United Kingdom' }
										);
										break;
									case EAnalyticsGeoRegion.WORLD:
										conditions.push(
												{ direction: EAnalyticsGeoLogicDirection.EXCLUDE, field: 'geo.country', value: 'United Kingdom' }
										);
										break;
								}
								
								return await this.databaseService.listGeoHitsCity(
										conditions,
										limit,
										range
								);
							},
							CommonsExpress.getQueryDateRange(req)
					);
				}
		);

		super.getHandler(
				`${path}statistics/geo/hits/country/:which(${regions.join('|')})`,
				async (req: ICommonsRequestWithStrictParams, _res: express.Response): Promise<TAnalyticsGeoHitCountry[]> => {
					const limit: number = CommonsType.hasPropertyNumber(req.query, 'limit') ? parseInt(req.query.limit as string, 10) : 100;
					
					const which: string|undefined = toEAnalyticsGeoRegion(req.strictParams.which as string);
					if (!which) return CommonsRestApi.badRequest('Invalid which. This should not be possible');
					
					return this.getCacheOrRenewDateRange<TAnalyticsGeoHitCountry[]>(
							`statistics/geo/hits/country/${which}/${limit.toString(10)}`,
							60,
							async (range?: TDateRange): Promise<TAnalyticsGeoHitCountry[]> => {
								const conditions: any = [];
								switch (which) {
									case EAnalyticsGeoRegion.UK:
										conditions.push(
												{ direction: EAnalyticsGeoLogicDirection.INCLUDE, field: 'geo.country', value: 'United Kingdom' }
										);
										break;
									case EAnalyticsGeoRegion.WORLD:
										conditions.push(
												{ direction: EAnalyticsGeoLogicDirection.EXCLUDE, field: 'geo.country', value: 'United Kingdom' }
										);
										break;
								}
								
								return await this.databaseService.listGeoHitsCountry(
										conditions,
										limit,
										range
								);
							},
							CommonsExpress.getQueryDateRange(req)
					);
				}
		);

		super.getHandler(
				`${path}statistics/devices/types`,
				async (req: express.Request, _res: express.Response): Promise<TAnalyticsDeviceTypeHits[]> => {
					return this.getCacheOrRenewDateRange<TAnalyticsDeviceTypeHits[]>(
							`statistics/devices/types`,
							60,
							async (range?: TDateRange): Promise<TAnalyticsDeviceTypeHits[]> => {
								return await this.databaseService.listDeviceTypeHits(range);
							},
							CommonsExpress.getQueryDateRange(req)
					);
				}
		);

		super.getHandler(
				`${path}statistics/devices/cpus`,
				async (req: express.Request, _res: express.Response): Promise<TAnalyticsDeviceCpuHits[]> => {
					return this.getCacheOrRenewDateRange<TAnalyticsDeviceCpuHits[]>(
							`statistics/devices/cpus`,
							60,
							async (range?: TDateRange): Promise<TAnalyticsDeviceCpuHits[]> => {
								return await this.databaseService.listDeviceCpuHits(range);
							},
							CommonsExpress.getQueryDateRange(req)
					);
				}
		);

		super.getHandler(
				`${path}statistics/devices/devices`,
				async (req: express.Request, _res: express.Response): Promise<TEncodedArray> => {
					return this.getCacheOrRenewDateRange<TEncodedArray>(
							`statistics/devices/devices`,
							60,
							async (range?: TDateRange): Promise<TEncodedArray> => {
								return CommonsType.encode(await this.databaseService.listDeviceHits(range)) as TEncodedArray;
							},
							CommonsExpress.getQueryDateRange(req)
					);
				}
		);

		super.getHandler(
				`${path}statistics/devices/engines`,
				async (req: express.Request, _res: express.Response): Promise<TEncodedArray> => {
					return this.getCacheOrRenewDateRange<TEncodedArray>(
							`statistics/devices/engines`,
							60,
							async (range?: TDateRange): Promise<TEncodedArray> => {
								return CommonsType.encode(await this.databaseService.listDeviceEngineHits(range)) as TEncodedArray;
							},
							CommonsExpress.getQueryDateRange(req)
					);
				}
		);

		super.getHandler(
				`${path}statistics/devices/browsers`,
				async (req: express.Request, _res: express.Response): Promise<TEncodedArray> => {
					return this.getCacheOrRenewDateRange<TEncodedArray>(
							`statistics/devices/browsers`,
							60,
							async (range?: TDateRange): Promise<TEncodedArray> => {
								return CommonsType.encode(await this.databaseService.listDeviceBrowserHits(range)) as TEncodedArray;
							},
							CommonsExpress.getQueryDateRange(req)
					);
				}
		);
		
		super.getHandler(
				`${path}statistics/devices/oss`,
				async (req: express.Request, _res: express.Response): Promise<TEncodedArray> => {
					return this.getCacheOrRenewDateRange<TEncodedArray>(
							`statistics/devices/oss`,
							60,
							async (range?: TDateRange): Promise<TEncodedArray> => {
								return CommonsType.encode(await this.databaseService.listDeviceOsHits(range)) as TEncodedArray;
							},
							CommonsExpress.getQueryDateRange(req)
					);
				}
		);
	}
	
	private async rebuildUniqueTally(base: TAnalyticsTimestampTally[], interval: number): Promise<TAnalyticsTimestampTally[]> {
		// There is no easy way to calculate unique hits hourly, as the requirement to group by hour vs distinct session id creates a contradictory situation.
		// Instead, perform a regular total lookup, and then cycle through each hour returned to do a range session distinct check.
		// It's slow and inefficient, but it's the only easy way to do it.
		// The cache is useful to help with this.
		
		const rebuild: TAnalyticsTimestampTally[] = [];
		
		// can't use a direct map sadly due to the async requirement
		for (const b of base) {
			if (b.tally === 0) {
				rebuild.push(b);
				continue;
			}
			
			const start: Date = b.timestamp;
			const end: Date = new Date(start.getTime() + interval);
			
			const unique: number = await this.databaseService.getUniqueHitsRange({ from: start, to: end });
			
			b.tally = unique;
			
			rebuild.push(b);
		}
		
		return rebuild;
	}
}
