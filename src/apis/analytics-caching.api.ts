import { CommonsDate } from 'tscommons-core';
import { TDateRange } from 'tscommons-core';

import { CommonsRestServer } from 'nodecommons-rest';
import { CommonsRestCachingApi } from 'nodecommons-rest-caching';

export class AnalyticsCachingApi extends CommonsRestCachingApi {
	private static buildIndex(path: string, range?: TDateRange): string {
		const components: string[] = [ `path:${path}` ];
		if (range) components.push(`${CommonsDate.dateToCompressed(range.from)}-${CommonsDate.dateToCompressed(range.to)}`);
		
		return components.join(';');
	}

	constructor(
			restServer: CommonsRestServer,
			memcachedPrefix: string
	) {
		super(restServer, memcachedPrefix);
	}
	
	protected async getCacheOrRenewDateRange<T>(
			subPath: string,
			age: number,
			callback: (range?: TDateRange) => Promise<T>,
			range?: TDateRange
	): Promise<T> {
		const index: string = AnalyticsCachingApi.buildIndex(subPath, range);
		
		return await super.getCacheOrRenew<T>(
				index,
				age,
				(): Promise<T> => callback(range)
		);
	}
}
