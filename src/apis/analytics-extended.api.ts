import * as express from 'express';

import { CommonsExpress } from 'nodecommons-express';
import { ICommonsRequestWithStrictParams } from 'nodecommons-express';
import { CommonsRestApi, CommonsRestServer } from 'nodecommons-rest';

import { AnalyticsDebug } from '../classes/analytics-debug';

import { TAnalyticsExtendedSizes } from '../types/tanalytics-extended-sizes';

export class AnalyticsExtendedApi extends CommonsRestApi {
	protected debug: AnalyticsDebug;
	private sizesCallbacks: ((uid: string, sizes: TAnalyticsExtendedSizes) => void)[] = [];

	constructor(
			restServer: CommonsRestServer,
			path: string,
			private setSizes: (
					uid: string,
					sizes: TAnalyticsExtendedSizes
			) => Promise<boolean>
	) {
		super(restServer);
		
		this.debug = new AnalyticsDebug();
		
		this.getHandler(`${path}sizes/:uid[base62]/:dw[int]/:dh[int]/:vw[int]/:vh[int]`, async (
				request: ICommonsRequestWithStrictParams,
				response: express.Response
		): Promise<{}> => {
			CommonsExpress.noCache(response);
			
			// help stop CORB warnings
			// NB doesn't seem to work, but doesn't do any harm to leave it in
			response.setHeader('X-Content-Type-Options', 'nosniff');

			// don't await so that the browser's request completes immediately
			this.sizes(request);
			
			return { r: 1 };
		});
		
		this.postHandler(`${path}sizes/:uid[base62]/:dw[int]/:dh[int]/:vw[int]/:vh[int]`, async (
				request: ICommonsRequestWithStrictParams,
				response: express.Response
		): Promise<{}> => {
			CommonsExpress.noCache(response);
			
			// help stop CORB warnings
			// NB doesn't seem to work, but doesn't do any harm to leave it in
			response.setHeader('X-Content-Type-Options', 'nosniff');

			// don't await so that the browser's request completes immediately
			this.sizes(request);
			
			return { r: 1 };
		});
	}

	public setDebug(enabled: boolean): void {
		this.debug.setEnabled(enabled);
	}
	
	public addSizesCallback(
			callback: (uid: string, sizes: TAnalyticsExtendedSizes) => void
	) {
		this.sizesCallbacks.push(callback);
	}
	
	private async sizes(
			request: ICommonsRequestWithStrictParams
	): Promise<void> {
		const sizes: TAnalyticsExtendedSizes = {
				deviceWidth: request.strictParams.dw as number,
				deviceHeight: request.strictParams.dh as number,
				viewportWidth: request.strictParams.vw as number,
				viewportHeight: request.strictParams.vh as number
		};
		
		this.debug.sizes(
				request.strictParams.uid as string,
				sizes
		);
		const outcome: boolean = await this.setSizes(
				request.strictParams.uid as string,
				sizes
		);
		
		if (outcome) {
			for (const callback of this.sizesCallbacks) {
				callback(
						request.strictParams.uid as string,
						sizes
				);
			}
		}
	}
}
