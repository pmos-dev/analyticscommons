import * as express from 'express';

import { CommonsType } from 'tscommons-core';
import { COMMONS_REGEX_PATTERN_IP } from 'tscommons-core';

import { CommonsOutput } from 'nodecommons-cli';
import { CommonsExpress } from 'nodecommons-express';
import { ICommonsRequestWithStrictParams } from 'nodecommons-express';
import { CommonsRestApi, CommonsRestServer } from 'nodecommons-rest';

import { AnalyticsDebug } from '../classes/analytics-debug';

export class AnalyticsXffPatchApi extends CommonsRestApi {
	protected debug: AnalyticsDebug;

	constructor(
			restServer: CommonsRestServer,
			private path: string,
			sendXffIpPatchToAnalytics?: (uid: string, ip: string) => Promise<boolean>,
			setIpAddressForUid?: (uid: string, ip: string) => Promise<boolean>
	) {
		super(restServer);
		
		this.debug = new AnalyticsDebug();
		
		// outgoing from browser to XffPatchApp
		if (sendXffIpPatchToAnalytics) {
			this.postHandler(`${this.path}xff-patch/:uid[base62]`, async (
					request: ICommonsRequestWithStrictParams,
					response: express.Response
			): Promise<{}> => {
				if (!sendXffIpPatchToAnalytics) return CommonsRestApi.notImplemented('sendXffIpPatchToAnalytics not implemented');
	
				CommonsExpress.noCache(response);
			
				// help stop CORB warnings
				// NB doesn't seem to work, but doesn't do any harm to leave it in
				response.setHeader('X-Content-Type-Options', 'nosniff');
	
				const uid: string = request.strictParams.uid as string;
	
				// we actually use false for the real parameter, as the 'real' IP address is nginx.
				const ip: string|undefined = CommonsExpress.getIpAddress(request);
				
				CommonsOutput.debug(`Resolved IP for ${uid} to be ${ip}`);
	
				if (ip) {
					// don't await so that the browser's request completes immediately
					sendXffIpPatchToAnalytics(uid, ip);
				}
	
				return { r: 1 };
			});
		}
		
		// incoming from XffPatchApp to Analytics
		if (setIpAddressForUid) {
			this.patchHandler(`${this.path}xff-patch/:uid[base62]`, async (
					request: ICommonsRequestWithStrictParams,
					response: express.Response
			): Promise<{}> => {
				if (!setIpAddressForUid) return CommonsRestApi.notImplemented('setIpAddressForUid not implemented');
				
				CommonsExpress.noCache(response);
			
				// help stop CORB warnings
				// NB doesn't seem to work, but doesn't do any harm to leave it in
				response.setHeader('X-Content-Type-Options', 'nosniff');
	
				const uid: string = request.strictParams.uid as string;
	
				// don't await
				if (
						CommonsType.hasPropertyString(request.body, 'ip')
						&& COMMONS_REGEX_PATTERN_IP.test(request.body.ip)
				) {
					CommonsOutput.debug(`Received from XffPatch IP for ${uid} to be ${request.body.ip}`);
					setIpAddressForUid(uid, request.body.ip);
				}
	
				return { r: 1 };
			});
		}
	}

	public setDebug(enabled: boolean): void {
		this.debug.setEnabled(enabled);
	}
}
