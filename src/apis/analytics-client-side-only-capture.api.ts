import * as express from 'express';

import { CommonsExpressServer } from 'nodecommons-express';
import { CommonsRestApi, CommonsRestServer } from 'nodecommons-rest';

export class AnalyticsClientSideOnlyCaptureApi extends CommonsRestApi {
	constructor(
			restServer: CommonsRestServer,
			private clientSideOnlyCaptureJsCode: string,
			private path: string
	) {
		super(restServer);
	}
	
	// have to do this directly with CommonsExpressServer, as the REST methods only work with JSON
	public installRequestHandlers(
			expressServer: CommonsExpressServer
	): void {
		expressServer.get(`${this.path}js/client-side-only-capture.js`, async (
				_request: express.Request,
				response: express.Response
		): Promise<void> => {
			response.setHeader('Content-Type', 'application/javascript');
			response.setHeader('Cache-Control', 'public, max-age=3600, s-maxage=3600');
			response.status(200).send(this.clientSideOnlyCaptureJsCode);
		});
	}
}
