import * as express from 'express';

import { IAnalyticsDevice } from 'tscommons-analytics';
import { IAnalyticsHit } from 'tscommons-analytics';
import { EAnalyticsCaptureMethod } from 'tscommons-analytics';

import { CommonsExpress, CommonsExpressServer } from 'nodecommons-express';
import { ICommonsRequestWithStrictParams } from 'nodecommons-express';
import { CommonsRestApi, CommonsRestServer } from 'nodecommons-rest';
import { COMMONS_IMAGE_PIXEL_JPEG, COMMONS_IMAGE_PIXEL_PNG } from 'nodecommons-image';

import { AnalyticsGeoIp } from '../classes/analytics-geo-ip';
import { AnalyticsDebug } from '../classes/analytics-debug';
import { AnalyticsDevice } from '../classes/analytics-device';

const ANALYTICS_TEST_HTML: string = `<!DOCTYPE html>
<html lang="en">
<body>
	<img src="/capture/12345678.jpg">
</body>
</html>`;

export class AnalyticsCaptureApi<H extends IAnalyticsHit> extends CommonsRestApi {
	protected debug: AnalyticsDebug;
	private captureCallbacks: ((hit: H) => void)[] = [];
	private associateCallbacks: ((uid: string, session: string) => void)[] = [];
	private unloadCallbacks: ((uid: string, y: number) => void)[] = [];
	
	private ignoreIps: string[] = [];

	constructor(
			restServer: CommonsRestServer,
			private geoIp: AnalyticsGeoIp,
			private captureJsCode: string,
			private path: string,
			private isAcceptedCallback: (base: IAnalyticsHit, request: express.Request) => boolean,
			private extendHitCallback: (base: IAnalyticsHit, request: express.Request) => H,
			private logHit: (hit: H) => Promise<boolean>,
			private associateHitWithSession: (
					uid: string,
					session: string
			) => Promise<boolean>,
			private logUnload: (
					uid: string,
					y: number|undefined
			) => Promise<boolean>
	) {
		super(restServer);
		
		this.debug = new AnalyticsDebug();
		
		this.getHandler(`${this.path}capture/:uid[base62].json`, async (
				request: ICommonsRequestWithStrictParams,
				response: express.Response
		): Promise<{}> => {
			CommonsExpress.noCache(response);
		
			// help stop CORB warnings
			// NB doesn't seem to work, but doesn't do any harm to leave it in
			response.setHeader('X-Content-Type-Options', 'nosniff');

			// don't await so that the browser's request completes immediately
			this.consider(request, EAnalyticsCaptureMethod.JSON);

			return { r: 1 };
		});
		
		this.getHandler(`${this.path}associate/:uid[base62]/:session[base62]`, async (
				request: ICommonsRequestWithStrictParams,
				response: express.Response
		): Promise<{}> => {
			CommonsExpress.noCache(response);
			
			// help stop CORB warnings
			// NB doesn't seem to work, but doesn't do any harm to leave it in
			response.setHeader('X-Content-Type-Options', 'nosniff');

			// don't await so that the browser's request completes immediately
			this.associate(request);
			
			return { r: 1 };
		});
		this.postHandler(`${this.path}associate/:uid[base62]/:session[base62]`, async (
				request: ICommonsRequestWithStrictParams,
				response: express.Response
		): Promise<{}> => {
			CommonsExpress.noCache(response);
			
			// help stop CORB warnings
			// NB doesn't seem to work, but doesn't do any harm to leave it in
			response.setHeader('X-Content-Type-Options', 'nosniff');

			// don't await so that the browser's request completes immediately
			this.associate(request);
			
			return { r: 1 };
		});
		
		this.getHandler(`${this.path}unload/:uid[base62]/:y[idname]`, async (
				request: ICommonsRequestWithStrictParams,
				response: express.Response
		): Promise<{}> => {
			CommonsExpress.noCache(response);
			
			// help stop CORB warnings
			// NB doesn't seem to work, but doesn't do any harm to leave it in
			response.setHeader('X-Content-Type-Options', 'nosniff');

			// don't await so that the browser's request completes immediately
			this.unload(request);
			
			return { r: 1 };
		});
		this.postHandler(`${this.path}unload/:uid[base62]/:y[idname]`, async (
				request: ICommonsRequestWithStrictParams,
				response: express.Response
		): Promise<{}> => {
			CommonsExpress.noCache(response);
			
			// help stop CORB warnings
			// NB doesn't seem to work, but doesn't do any harm to leave it in
			response.setHeader('X-Content-Type-Options', 'nosniff');

			// don't await so that the browser's request completes immediately
			this.unload(request);
			
			return { r: 1 };
		});
	}

	public setDebug(enabled: boolean): void {
		this.debug.setEnabled(enabled);
	}
	
	public setIgnoreIps(ips: string[]): void {
		this.ignoreIps = ips;
	}
	
	public addAcceptedHitCallback(
			callback: (hit: H) => void
	) {
		this.captureCallbacks.push(callback);
	}
	
	public addAssociateCallback(
			callback: (uid: string, session: string) => void
	) {
		this.associateCallbacks.push(callback);
	}
	
	public addUnloadCallback(
			callback: (uid: string, y: number) => void
	) {
		this.unloadCallbacks.push(callback);
	}
	
	// have to do this directly with CommonsExpressServer, as the REST methods only work with JSON
	public installRequestHandlers(
			expressServer: CommonsExpressServer
	): void {
		expressServer.get(`${this.path}capture/:uid[base62].jpg`, async (
				request: ICommonsRequestWithStrictParams,
				response: express.Response
		): Promise<void> => {
			CommonsExpress.noCache(response);
			response.setHeader('Content-Type', 'image/jpeg');
			response.status(200).send(COMMONS_IMAGE_PIXEL_JPEG);
			
			await this.consider(request, EAnalyticsCaptureMethod.JPEG);
		});
		
		expressServer.get(`${this.path}capture/:uid[base62].png`, async (
				request: ICommonsRequestWithStrictParams,
				response: express.Response
		): Promise<void> => {
			CommonsExpress.noCache(response);
			response.setHeader('Content-Type', 'image/png');
			response.status(200).send(COMMONS_IMAGE_PIXEL_PNG);
			
			await this.consider(request, EAnalyticsCaptureMethod.PNG);
		});
		
		expressServer.get(`${this.path}js/capture.js`, async (
				_request: express.Request,
				response: express.Response
		): Promise<void> => {
			response.setHeader('Content-Type', 'application/javascript');
			response.setHeader('Cache-Control', 'public, max-age=3600, s-maxage=3600');
			response.status(200).send(this.captureJsCode);
		});
		
		expressServer.get(`${this.path}test/jpg`, async (
				_request: express.Request,
				response: express.Response
		): Promise<void> => {
			CommonsExpress.noCache(response);
			response.setHeader('Content-Type', 'text/html');
			response.status(200).send(ANALYTICS_TEST_HTML);
		});
	}
	
	private async consider(
			request: ICommonsRequestWithStrictParams,
			method: EAnalyticsCaptureMethod
	): Promise<void> {
		// we actually use false for the real parameter, as the 'real' IP address is nginx.
		let ip: string|undefined = CommonsExpress.getIpAddress(request);
		
		// the XffPatch approach means we should ignore 'invalid' load balancer IPs
		if (ip && this.ignoreIps.includes(ip)) ip = undefined;

		const now: Date = new Date();

		const hour: Date = new Date(now.getTime());
		hour.setMinutes(0);
		hour.setSeconds(0);
		hour.setMilliseconds(0);
		
		const day: Date = new Date(now.getTime());
		day.setHours(0);
		day.setMinutes(0);
		day.setSeconds(0);
		day.setMilliseconds(0);

		const base: IAnalyticsHit = {
				timestamp: now,
				day: day,
				hour: hour,
				captureMethod: method,
				uid: request.strictParams.uid as string
		};
		
		if (ip) base.geo = this.geoIp.getGeo(ip);

		const userAgent: string|undefined = CommonsExpress.getUserAgent(request);
		if (userAgent) {
			const device: IAnalyticsDevice|undefined = AnalyticsDevice.detect(userAgent);
			if (device) base.device = device;
		}
		
		if (!this.isAcceptedCallback(base, request)) {
			this.debug.reject();
			return;
		}
		
		const extended: H = this.extendHitCallback(base, request);
		
		this.debug.log(extended);
		
		const outcome: boolean = await this.logHit(extended);
		
		if (outcome) {
			for (const callback of this.captureCallbacks) callback(extended);
		}
	}

	private async associate(
			request: ICommonsRequestWithStrictParams
	): Promise<void> {
		this.debug.associate(
				request.strictParams.uid as string,
				request.strictParams.session as string
		);
		const outcome: boolean = await this.associateHitWithSession(
				request.strictParams.uid as string,
				request.strictParams.session as string
		);
		
		if (outcome) {
			for (const callback of this.associateCallbacks) {
				callback(
						request.strictParams.uid as string,
						request.strictParams.session as string
				);
			}
		}
	}
	
	private async unload(
			request: ICommonsRequestWithStrictParams
	): Promise<void> {
		let y: number|undefined;
		if (request.strictParams.y !== 'na') {
			try {
				y = parseInt(request.strictParams.y as string, 10);
			} catch (e) {
				// ignore
			}
		}
		
		this.debug.unload(request.strictParams.uid as string);
		
		const outcome: boolean = await this.logUnload(
				request.strictParams.uid as string,
				y
		);
		
		if (outcome && y !== undefined) {
			for (const callback of this.unloadCallbacks) {
				callback(
						request.strictParams.uid as string,
						y
				);
			}
		}
	}
}
