export type TAnalyticsExtendedSizes = {
		deviceWidth: number;
		deviceHeight: number;
		viewportWidth: number;
		viewportHeight: number;
};
