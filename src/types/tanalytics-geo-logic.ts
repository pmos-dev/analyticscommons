import { EAnalyticsGeoLogicDirection } from '../enums/eanalytics-geo-logic-direction';

export type TAnalyticsGeoLogic = {
		direction: EAnalyticsGeoLogicDirection;
		field: string;
		value: string;
};
