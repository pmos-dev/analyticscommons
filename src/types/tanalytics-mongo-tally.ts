import { CommonsType } from 'tscommons-core';

export type TAnalyticsMongoTally<T> = {
		_id: T;
		tally: number;
};

export function isTAnalyticsMongoTally<T>(
		test: unknown,
		isT: (t: unknown) => t is T
): test is TAnalyticsMongoTally<T> {
	if (!CommonsType.hasProperty(test, '_id')) return false;
	if (!isT(test['_id'])) return false;
	
	if (!CommonsType.hasPropertyNumber(test, 'tally')) return false;
	
	return true;
}

export type TAnalyticsMongoStringTally = TAnalyticsMongoTally<string>;

export function isTAnalyticsMongoStringTally(
		test: unknown
): test is TAnalyticsMongoStringTally {
	return isTAnalyticsMongoTally<string>(
			test,
			CommonsType.isString
	);
}

export type TAnalyticsMongoDateTally = TAnalyticsMongoTally<Date>;

export function isTAnalyticsMongoDateTally(
		test: unknown
): test is TAnalyticsMongoDateTally {
	return isTAnalyticsMongoTally<Date>(
			test,
			CommonsType.isDate
	);
}
