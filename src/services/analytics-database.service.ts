import { Collection, AggregationCursor, Cursor } from 'mongodb';

import { CommonsType } from 'tscommons-core';
import { TDateRange } from 'tscommons-core';
import { IAnalyticsHit } from 'tscommons-analytics';
import { TAnalyticsGeoAggregate } from 'tscommons-analytics';
import { TAnalyticsTimestampTally } from 'tscommons-analytics';
import { TAnalyticsGeoHitCity } from 'tscommons-analytics';
import { TAnalyticsGeoHitCountry } from 'tscommons-analytics';
import { IAnalyticsGeo } from 'tscommons-analytics';
import { TAnalyticsDeviceHits } from 'tscommons-analytics';
import { TAnalyticsDeviceTypeHits } from 'tscommons-analytics';
import { TAnalyticsDeviceCpuHits } from 'tscommons-analytics';
import { TAnalyticsDeviceVersionedHits } from 'tscommons-analytics';
import { EAnalyticsDeviceType, toEAnalyticsDeviceType } from 'tscommons-analytics';
import { EAnalyticsCpu, toEAnalyticsCpu } from 'tscommons-analytics';
import { ICommonsGeographic, isICommonsGeographic } from 'tscommons-geographics';

import { ICommonsCredentials } from 'nodecommons-database';
import { CommonsMongodbService } from 'nodecommons-database-mongodb';

import { TAnalyticsGeoLogic } from '../types/tanalytics-geo-logic';
import { TAnalyticsExtendedSizes } from '../types/tanalytics-extended-sizes';
import {
		TAnalyticsMongoTally, isTAnalyticsMongoTally,
		TAnalyticsMongoDateTally, isTAnalyticsMongoDateTally,
		TAnalyticsMongoStringTally, isTAnalyticsMongoStringTally
} from '../types/tanalytics-mongo-tally';

import { EAnalyticsGeoLogicDirection } from '../enums/eanalytics-geo-logic-direction';

type TMongoGeoTally = TAnalyticsMongoTally<ICommonsGeographic>;
function isTMongoGeoTally(
		test: unknown
): test is TMongoGeoTally {
	return isTAnalyticsMongoTally<ICommonsGeographic>(
			test,
			isICommonsGeographic
	);
}

type TMongoDevice = {
		vendor: string|undefined;
		model: string|undefined;
};
function isTMongoDevice(
		test: unknown
): test is TMongoDevice {
	if (!CommonsType.hasPropertyStringOrUndefined(test, 'vendor')) return false;
	if (!CommonsType.hasPropertyStringOrUndefined(test, 'model')) return false;
	
	return true;
}

type TMongoEngine = {
		engine: string;
		version?: number;
};
function isTMongoEngine(
		test: unknown
): test is TMongoEngine {
	if (!CommonsType.hasPropertyString(test, 'engine')) return false;
	if (!CommonsType.hasPropertyNumberOrUndefined(test, 'version')) return false;
	
	return true;
}

type TMongoBrowser = {
		browser: string;
		version?: number;
};
function isTMongoBrowser(
		test: unknown
): test is TMongoBrowser {
	if (!CommonsType.hasPropertyString(test, 'browser')) return false;
	if (!CommonsType.hasPropertyNumberOrUndefined(test, 'version')) return false;
	
	return true;
}

type TMongoOs = {
		os: string;
		version?: number;
};
function isTMongoOs(
		test: unknown
): test is TMongoOs {
	if (!CommonsType.hasPropertyString(test, 'os')) return false;
	if (!CommonsType.hasPropertyNumberOrUndefined(test, 'version')) return false;
	
	return true;
}

export abstract class AnalyticsDatabaseService<H extends IAnalyticsHit> extends CommonsMongodbService {
	public static range(range: TDateRange): {}[] {
		return [
				{ timestamp: { $gte: range.from } },
				{ timestamp: { $lt: range.to } }
		];
	}
 	
	protected hits: Collection<H>|undefined;
	
	constructor(credentials: ICommonsCredentials) {
		super(credentials);
	}
	
	// This is a typescript generics hack: the Collection<H> isn't liked by some of the insert and update methods which can't guarantee which fields are present and which formats etc. due to possible subinterfaces
	private getHits(): Collection<IAnalyticsHit> {
		if (!this.hits) throw new Error('Hits is not instantiated');

		return this.hits as unknown as Collection<IAnalyticsHit>;
	}
	
	public async init(): Promise<void> {
		await super.init();

		this.hits = await this.ensureCollection('hits');
		await this.hits.createIndex({ uid: 1 }, { unique: false });
		await this.hits.createIndex({ session: 1 }, { unique: false });
		await this.hits.createIndex({ timestamp: 1 }, { unique: false });
		await this.hits.createIndex({ day: 1 }, { unique: false });
		await this.hits.createIndex({ hour: 1 }, { unique: false });
		await this.hits.createIndex({ 'geo.location.longitude': 1, 'geo.location.latitude': 1 }, { unique: false });
		await this.hits.createIndex({ 'geo.city': 1 }, { unique: false });
		await this.hits.createIndex({ 'geo.country': 1 }, { unique: false });
		await this.hits.createIndex({ 'device.type': 1 }, { unique: false });
		await this.hits.createIndex({ 'device.device': 1 }, { unique: false });
		await this.hits.createIndex({ 'device.browser': 1 }, { unique: false });
		await this.hits.createIndex({ 'device.os': 1 }, { unique: false });
	}
	
	public async log(hit: H): Promise<boolean> {
		try {
			await this.getHits().insertOne(hit);
			
			return true;
		} catch (e) {
			console.error(e);
			return false;
		}
	}
	
	public async logMany(hits: H[]): Promise<boolean> {
		try {
			await this.getHits().insertMany(hits);
			
			return true;
		} catch (e) {
			console.error(e);
			return false;
		}
	}
	
	public async associate(uid: string, session: string): Promise<boolean> {
		try {
			await this.getHits().updateMany(
					{
						uid: uid
					},
					{
						$set: { session: session }
					}
			);
			
			return true;
		} catch (e) {
			console.error(e);
			return false;
		}
	}
	
	public async setUnload(uid: string, y: number|undefined): Promise<boolean> {
		try {
			await this.getHits().updateOne(
					{
						uid: uid,
						unload: { $exists: false }
					},
					{
						$set: {
							unload: new Date(),
							y: y === undefined ? null : y
						}
					}
			);
			
			return true;
		} catch (e) {
			console.error(e);
			return false;
		}
	}
	
	public async setSizes(uid: string, sizes: TAnalyticsExtendedSizes): Promise<boolean> {
		try {
			await this.getHits().updateOne(
					{
						uid: uid,
						sizes: { $exists: false }
					},
					{
						$set: {
							sizes: sizes
						}
					}
			);
			
			return true;
		} catch (e) {
			console.error(e);
			return false;
		}
	}
	
	public async setGeo(uid: string, geo: IAnalyticsGeo): Promise<boolean> {
		try {
			await this.getHits().updateOne(
					{
						uid: uid
					},
					{
						$set: {
							geo: geo
						}
					}
			);
			
			return true;
		} catch (e) {
			console.error(e);
			return false;
		}
	}
	
	public async getTotalHits(): Promise<number> {
		return await this.getHits().countDocuments();
	}
	
	public async getUniqueHits(): Promise<number> {
		return (await this.getHits().distinct('session')).length;
	}
	
	public async getUnknownHits(): Promise<number> {
		return await this.getHits().find({ session: { $exists: false }}).count();
	}
	
	public async getTotalHitsRange(range: TDateRange): Promise<number> {
		return await this.getHits().find(
				{ $and: AnalyticsDatabaseService.range(range) }
		).count();
	}
	
	public async getUniqueHitsRange(range: TDateRange): Promise<number> {
		return (await this.getHits().distinct(
				'session',
				{ $and: [
						{ session: { $exists: true } },
						...AnalyticsDatabaseService.range(range)
				] }
		)).length;
	}
	
	public async getUnknownHitsRange(range: TDateRange): Promise<number> {
		return await this.getHits().find(
				{ $and: [
						{ session: { $exists: false } },
						...AnalyticsDatabaseService.range(range)
				] }
		).count();
	}

	protected async convertTimestampTally(results: AggregationCursor<TAnalyticsMongoDateTally>): Promise<TAnalyticsTimestampTally[]> {
		return (await this.listQueryResults(
				results,
				isTAnalyticsMongoDateTally
		))
				.map((row: TAnalyticsMongoDateTally): TAnalyticsTimestampTally => ({
						timestamp: row._id,
						tally: row.tally
				}));
	}

	public async listHourlyTotalHits(): Promise<TAnalyticsTimestampTally[]> {
		const count: TAnalyticsTimestampTally[] = await this.convertTimestampTally(
				this.getHits().aggregate<TAnalyticsMongoDateTally>([
						{ $group: {
							_id: '$hour',
							tally: { $sum: 1 }
						}}
				])
		);
		
		return count;
	}
 	
	public async listDailyTotalHits(): Promise<TAnalyticsTimestampTally[]> {
		const count: TAnalyticsTimestampTally[] = await this.convertTimestampTally(
				this.getHits().aggregate<TAnalyticsMongoDateTally>([
						{ $group: {
							_id: '$day',
							tally: { $sum: 1 }
						}}
				])
		);
		
		return count;
	}
	
	public async listHourlyTotalHitsRange(range: TDateRange): Promise<TAnalyticsTimestampTally[]> {
		const count: TAnalyticsTimestampTally[] = await this.convertTimestampTally(
				this.getHits().aggregate<TAnalyticsMongoDateTally>([
						{ $match: { $and: AnalyticsDatabaseService.range(range) } },
						{ $group: {
							_id: '$hour',
							tally: { $sum: 1 }
						}}
				])
		);
		
		return count;
	}
	
	public async listDailyTotalHitsRange(range: TDateRange): Promise<TAnalyticsTimestampTally[]> {
		const count: TAnalyticsTimestampTally[] = await this.convertTimestampTally(
				this.getHits().aggregate<TAnalyticsMongoDateTally>([
						{ $match: { $and: AnalyticsDatabaseService.range(range) } },
						{ $group: {
							_id: '$day',
							tally: { $sum: 1 }
						}}
				])
		);
		
		return count;
	}
	
	public async listHourlyUnknownHits(): Promise<TAnalyticsTimestampTally[]> {
		const count: TAnalyticsTimestampTally[] = await this.convertTimestampTally(
				this.getHits().aggregate<TAnalyticsMongoDateTally>([
						{ $match: { session: { $exists: false } } },
						{ $group: {
							_id: '$hour',
							tally: { $sum: 1 }
						}}
				])
		);
		
		return count;
	}
 	
	public async listDailyUnknownHits(): Promise<TAnalyticsTimestampTally[]> {
		const count: TAnalyticsTimestampTally[] = await this.convertTimestampTally(
				this.getHits().aggregate<TAnalyticsMongoDateTally>([
						{ $match: { session: { $exists: false } } },
						{ $group: {
							_id: '$day',
							tally: { $sum: 1 }
						}}
				])
		);
		
		return count;
	}
	
	public async listHourlyUnknownHitsRange(range: TDateRange): Promise<TAnalyticsTimestampTally[]> {
		const count: TAnalyticsTimestampTally[] = await this.convertTimestampTally(
				this.getHits().aggregate<TAnalyticsMongoDateTally>([
						{ $match: { $and: [ { session: { $exists: false } }, ...AnalyticsDatabaseService.range(range) ] } },
						{ $group: {
							_id: '$hour',
							tally: { $sum: 1 }
						}}
				])
		);
		
		return count;
	}
	
	public async listDailyUnknownHitsRange(range: TDateRange): Promise<TAnalyticsTimestampTally[]> {
		const count: TAnalyticsTimestampTally[] = await this.convertTimestampTally(
				this.getHits().aggregate<TAnalyticsMongoDateTally>([
						{ $match: { $and: [ { session: { $exists: false } }, ...AnalyticsDatabaseService.range(range) ] } },
						{ $group: {
							_id: '$day',
							tally: { $sum: 1 }
						}}
				])
		);
		
		return count;
	}
	
	public async listGeoAggregate(
			granularity: number,
			logics: TAnalyticsGeoLogic[] = [],
			range?: TDateRange
	): Promise<TAnalyticsGeoAggregate[]> {
		const filters: {}[] = [
				{ 'geo.location': { $exists: true } }
		];
		if (range) filters.push(...AnalyticsDatabaseService.range(range));
		
		for (const logic of logics) {
			const filter: {} = {};
			filter[logic.field] = logic.direction === EAnalyticsGeoLogicDirection.INCLUDE ? logic.value : { $ne: logic.value };
			
			filters.push(filter);
		}
		
		const results: AggregationCursor<TMongoGeoTally> = this.getHits().aggregate<TMongoGeoTally>([
				{ $match: { $and: filters } },
				{ $group: {
						_id: {
								longitude: {
										$add: [
												{ $divide: [
														{ $trunc: {
																$multiply: [
																		'$geo.location.longitude',
																		granularity
																]
														} },
														granularity
												] },
												0
										]
								},
								latitude: {
										$add: [
												{ $divide: [
														{ $trunc: {
																$multiply: [
																		'$geo.location.latitude',
																		granularity
																]
														} },
														granularity
												] },
												0
										]
								}
						},
						tally: { $sum: 1 }
				} }
		]);

		return (await this.listQueryResults<TMongoGeoTally>(results, isTMongoGeoTally))
				.map((row: TMongoGeoTally): TAnalyticsGeoAggregate => {
					return {
							longitude: row._id.longitude,
							latitude: row._id.latitude,
							t: row.tally
					};
				});
	}

	public async listGeoHitsCity(
			logics: TAnalyticsGeoLogic[] = [],
			limit: number = 100,
			range?: TDateRange,
			includeNone: boolean = false
	): Promise<TAnalyticsGeoHitCity[]> {
		const filters: {}[] = [
				{ 'geo.city': { $exists: true, $ne: '' } }
		];
		if (range) filters.push(...AnalyticsDatabaseService.range(range));
		
		for (const logic of logics) {
			const filter: {} = {};
			filter[logic.field] = logic.direction === EAnalyticsGeoLogicDirection.INCLUDE ? logic.value : { $ne: logic.value };
			
			filters.push(filter);
		}
		
		const results: AggregationCursor<TAnalyticsMongoStringTally> = this.getHits().aggregate<TAnalyticsMongoStringTally>([
				{ $match: { $and: filters } },
				{ $group: {
						_id: '$geo.city',
						tally: { $sum: 1 }
				} },
				{ $sort: {
						tally: -1
				} },
				{ $limit: limit + (includeNone ? 0 : 1) }
		]);

		return (await this.listQueryResults<TAnalyticsMongoStringTally>(results, isTAnalyticsMongoStringTally))
				.map((row: TAnalyticsMongoStringTally): TAnalyticsGeoHitCity => ({
						city: row._id,
						tally: row.tally
				}))
				.filter((row: TAnalyticsGeoHitCity): boolean => row.city !== null)
				.slice(0, limit);
	}

	public async listGeoHitsCountry(
			logics: TAnalyticsGeoLogic[] = [],
			limit: number = 100,
			range?: TDateRange,
			includeNone: boolean = false
	): Promise<TAnalyticsGeoHitCountry[]> {
		const filters: {}[] = [
				{ 'geo.country': { $exists: true, $ne: '' } }
		];
		if (range) filters.push(...AnalyticsDatabaseService.range(range));
		
		for (const logic of logics) {
			const filter: {} = {};
			filter[logic.field] = logic.direction === EAnalyticsGeoLogicDirection.INCLUDE ? logic.value : { $ne: logic.value };
			
			filters.push(filter);
		}
		
		const results: AggregationCursor<TAnalyticsMongoStringTally> = this.getHits().aggregate<TAnalyticsMongoStringTally>([
				{ $match: { $and: filters } },
				{ $group: {
						_id: '$geo.country',
						tally: { $sum: 1 }
				} },
				{ $sort: {
						tally: -1
				} },
				{ $limit: limit + (includeNone ? 0 : 1) }
		]);

		return (await this.listQueryResults<TAnalyticsMongoStringTally>(results, isTAnalyticsMongoStringTally))
				.map((row: TAnalyticsMongoStringTally): TAnalyticsGeoHitCountry => ({
						country: row._id,
						tally: row.tally
				}))
				.filter((row: TAnalyticsGeoHitCountry): boolean => includeNone || row.country !== null)
				.slice(0, limit);
	}

	public async listDeviceTypeHits(range?: TDateRange): Promise<TAnalyticsDeviceTypeHits[]> {
		const filters: {}[] = [
				{ 'device.type': { $exists: true, $ne: null } }
		];
		if (range) filters.push(...AnalyticsDatabaseService.range(range));

		const results: AggregationCursor<TAnalyticsMongoStringTally> = this.getHits().aggregate<TAnalyticsMongoStringTally>([
				{ $match: { $and: filters } },
				{ $group: {
						_id: '$device.type',
						tally: { $sum: 1 }
				} }
		]);

		return (await this.listQueryResults<TAnalyticsMongoStringTally>(
				results,
				isTAnalyticsMongoStringTally
		))
				.map((row: TAnalyticsMongoStringTally): TAnalyticsDeviceTypeHits => ({
						type: CommonsType.assertEnum<EAnalyticsDeviceType>(row._id, toEAnalyticsDeviceType, 'EAnalyticsDeviceType'),
						tally: row.tally
				}))
				.filter((row: TAnalyticsDeviceTypeHits): boolean => row.type !== null);
	}

	public async listDeviceCpuHits(range?: TDateRange): Promise<TAnalyticsDeviceCpuHits[]> {
		const filters: {}[] = [
				{ 'device.cpu': { $exists: true, $ne: null } }
		];
		if (range) filters.push(...AnalyticsDatabaseService.range(range));

		const results: AggregationCursor<TAnalyticsMongoStringTally> = this.getHits().aggregate<TAnalyticsMongoStringTally>([
				{ $match: { $and: filters } },
				{ $group: {
						_id: '$device.cpu',
						tally: { $sum: 1 }
				} }
		]);

		return (await this.listQueryResults<TAnalyticsMongoStringTally>(
				results,
				isTAnalyticsMongoStringTally
		))
				.map((row: TAnalyticsMongoStringTally): TAnalyticsDeviceCpuHits => ({
						cpu: CommonsType.assertEnum<EAnalyticsCpu>(row._id, toEAnalyticsCpu, 'EAnalyticsCpu'),
						tally: row.tally
				}))
				.filter((row: TAnalyticsDeviceCpuHits): boolean => row.cpu !== null);
	}

	public async listDeviceHits(range?: TDateRange): Promise<TAnalyticsDeviceHits[]> {
		const filters: {}[] = [ {
				$or: [
						{ 'device.vendor': { $exists: true, $ne: null } },
						{ 'device.model': { $exists: true, $ne: null } }
				]
		} ];
		if (range) filters.push(...AnalyticsDatabaseService.range(range));

		const results: AggregationCursor<TAnalyticsMongoTally<TMongoDevice>> = this.getHits().aggregate<TAnalyticsMongoTally<TMongoDevice>>([
				{ $match: { $and: filters } },
				{ $group: {
						_id: { vendor: '$device.vendor', model: '$device.model' },
						tally: { $sum: 1 }
				} }
		]);
		
		return (await this.listQueryResults<TAnalyticsMongoTally<TMongoDevice>>(
				results,
				(test: unknown): test is TAnalyticsMongoTally<TMongoDevice> => isTAnalyticsMongoTally<TMongoDevice>(
						test,
						isTMongoDevice
				)
		))
				.filter((row: TAnalyticsMongoTally<TMongoDevice>): boolean => (row._id.vendor || row._id.model) ? true : false)
				.map((row: TAnalyticsMongoTally<TMongoDevice>): TAnalyticsDeviceHits => ({
						vendor: row._id.vendor || null,
						model: row._id.model || null,
						tally: row.tally
				}));
	}

	public async listDeviceEngineHits(range?: TDateRange): Promise<TAnalyticsDeviceVersionedHits[]> {
		const filters: {}[] = [
				{ 'device.engine': { $exists: true, $ne: null } }
		];
		if (range) filters.push(...AnalyticsDatabaseService.range(range));

		const results: AggregationCursor<TAnalyticsMongoTally<TMongoEngine>> = this.getHits().aggregate<TAnalyticsMongoTally<TMongoEngine>>([
				{ $match: { $and: filters } },
				{ $group: {
						_id: { engine: '$device.engine', version: '$device.engineVersion' },
						tally: { $sum: 1 }
				} }
		]);
		
		return (await this.listQueryResults<TAnalyticsMongoTally<TMongoEngine>>(
				results,
				(test: unknown): test is TAnalyticsMongoTally<TMongoEngine> => isTAnalyticsMongoTally<TMongoEngine>(
						test,
						isTMongoEngine
				)
		))
				.filter((row: TAnalyticsMongoTally<TMongoEngine>): boolean => row._id.engine ? true : false)
				.map((row: TAnalyticsMongoTally<TMongoEngine>): TAnalyticsDeviceVersionedHits => ({
						name: row._id.engine,
						version: row._id.version || null,
						tally: row.tally
				}));
	}

	public async listDeviceBrowserHits(range?: TDateRange): Promise<TAnalyticsDeviceVersionedHits[]> {
		const filters: {}[] = [
				{ 'device.browser': { $exists: true, $ne: null } }
		];
		if (range) filters.push(...AnalyticsDatabaseService.range(range));

		const results: AggregationCursor<TAnalyticsMongoTally<TMongoBrowser>> = this.getHits().aggregate<TAnalyticsMongoTally<TMongoBrowser>>([
				{ $match: { $and: filters } },
				{ $group: {
						_id: { browser: '$device.browser', version: '$device.browserVersion' },
						tally: { $sum: 1 }
				} }
		]);
		
		return (await this.listQueryResults<TAnalyticsMongoTally<TMongoBrowser>>(
				results,
				(test: unknown): test is TAnalyticsMongoTally<TMongoBrowser> => isTAnalyticsMongoTally<TMongoBrowser>(
						test,
						isTMongoBrowser
				)
		))
				.filter((row: TAnalyticsMongoTally<TMongoBrowser>): boolean => row._id.browser ? true : false)
				.map((row: TAnalyticsMongoTally<TMongoBrowser>): TAnalyticsDeviceVersionedHits => ({
						name: row._id.browser,
						version: row._id.version || null,
						tally: row.tally
				}));
	}

	public async listDeviceOsHits(range?: TDateRange): Promise<TAnalyticsDeviceVersionedHits[]> {
		const filters: {}[] = [
				{ 'device.os': { $exists: true, $ne: null } }
		];
		if (range) filters.push(...AnalyticsDatabaseService.range(range));

		const results: AggregationCursor<TAnalyticsMongoTally<TMongoOs>> = this.getHits().aggregate<TAnalyticsMongoTally<TMongoOs>>([
				{ $match: { $and: filters } },
				{ $group: {
						_id: { os: '$device.os', version: '$device.osVersion' },
						tally: { $sum: 1 }
				} }
		]);
		
		return (await this.listQueryResults<TAnalyticsMongoTally<TMongoOs>>(
				results,
				(test: unknown): test is TAnalyticsMongoTally<TMongoOs> => isTAnalyticsMongoTally<TMongoOs>(
						test,
						isTMongoOs
				)
		))
				.filter((row: TAnalyticsMongoTally<TMongoOs>): boolean => row._id.os ? true : false)
				.map((row: TAnalyticsMongoTally<TMongoOs>): TAnalyticsDeviceVersionedHits => ({
						name: row._id.os,
						version: row._id.version || null,
						tally: row.tally
				}));
	}
	
	public export(): Cursor<H> {
		return this.getHits().find({});
	}
}
